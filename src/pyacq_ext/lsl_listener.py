"""Example program to demonstrate how to read a multi-channel time-series
from LSL in a chunk-by-chunk manner (which is more efficient)."""

from pylsl import StreamInlet, resolve_stream
import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
from pyqtgraph.util.mutex import Mutex

from pyacq.core import Node, register_node_type

_dtype_trigger = [('pos', 'int64'),
                ('points', 'int64'),
                ('channel', 'int64'),
                ('type', 'S16'),  # TODO check size
                ('description', 'S16'),  # TODO check size
                ]

class LslListenerThread(QtCore.QThread):
    sig_new_chunk = QtCore.pyqtSignal(int)

    def __init__(self, outputs, host, port, nb_channel, resolutions, parent=None):
        QtCore.QThread.__init__(self)
        self.outputs = outputs
        self.nb_channel = nb_channel
        self.resolutions = resolutions

        self.lock = Mutex()
        self.running = False

        self.parent = parent

    def run(self):
        with self.lock:
            self.running = True
        head = 0
        head_marker = 0
        streamsEEG = resolve_stream('type', 'EEG')

        # streamsMarkers = resolve_stream('type', 'Markers')
        # # create a new inlet to read from the stream
        # inletMarkers = StreamInlet(streamsMarkers[0])

        # create a new inlet to read from the stream
        inletEEG = StreamInlet(streamsEEG[0])

        while True:
            with self.lock:
                if not self.running:
                    break
            chunk, timestamps = inletEEG.pull_chunk()
            if timestamps:
                chunk = np.array(chunk)
                # MarkerDesc, timestamp_marker = inletMarkers.pull_sample()
                points = np.shape(chunk)[0]
                head += points
                self.outputs['signals'].send(chunk, index=head)

            # Extract markers
            # nb_marker = MarkerDesc.size()
            # markers = np.empty((nb_marker,), dtype=_dtype_trigger)
            # for m in range(nb_marker):
            #     markers['pos'][m] = timestamp_marker[m]
            #     markers['points'][m] = 0
            #     markers['channel'][m] = 0
            #     markers['type'][m] = b'Stimulus'
            #     markers['description'][m] = MarkerDesc[m]
            #
            # head_marker += nb_marker
            # markers['pos'] += (head - points)
            # self.outputs['triggers'].send(markers, index=nb_marker)

    def stop(self):
        with self.lock:
            self.running = False

class LslListener(Node):
    _output_specs = {'signals': dict(streamtype='analogsignal', dtype='float64',
                                     shape=(-1, 32), compression='', timeaxis=0,
                                     sample_rate=250.),
                     'triggers': dict(streamtype='event', dtype=_dtype_trigger,
                                      shape=(-1,)),
                     }

    def __init__(self, **kargs):
        Node.__init__(self, **kargs)

    def _configure(self, host='', port=-1):
        # assert msgtype == 1, 'First message from brainamp is not type 1'
        # first resolve an EEG stream on the lab network
        streams = resolve_stream('type', 'EEG')

        # create a new inlet to read from the stream
        inlet = StreamInlet(streams[0])
        info = inlet.info()

        self.nb_channel = info.channel_count()
        self.sample_rate = info.nominal_srate()
        self.channel_names =  ['Fp1', 'Fp2', 'F3', 'Fz', 'F4', 'C1', 'Cz', 'C2', 'P3', 'Pz', 'P4', 'O1', 'Oz', 'O2', 'T3', 'T4']



        self.outputs['signals'].spec['shape'] = (-1, self.nb_channel)
        self.outputs['signals'].spec['sample_rate'] = self.sample_rate
        self.outputs['signals'].spec['nb_channel'] = self.nb_channel

    def _initialize(self):
        self._thread = LslListenerThread(self.outputs, -1, -1,
                                      self.nb_channel, -1, parent=self)

    def after_output_configure(self, outputname):
        if outputname == 'signals':
            channel_info = [{'name': ch_name} for ch_name in self.channel_names]
            self.outputs[outputname].params['channel_info'] = channel_info

    def _start(self):
        self.chunkIndex = 0
        self._thread.start()

    def _stop(self):
        self._thread.stop()
        self._thread.wait()

    def _close(self):
        pass

register_node_type(LslListener)